package com.astronlab.framework.android.util;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.astronlab.framework.android.observers.listeners.CustomItemSelectionListener;
import com.astronlab.framework.android.observers.notifier.IItemSelectionNotifier;


/**
 * This class is used to fill/get contents of given spinner.
 */
public class SpinnerArrayAdapter extends ArrayAdapter<String> {

    Spinner spinner;

    public SpinnerArrayAdapter(Activity context, int spinnerViewId,
                               String selectionTitle) {
        super(context, android.R.layout.simple_spinner_item);
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = (Spinner) context.findViewById(spinnerViewId);
        spinner.setOnItemSelectedListener(new CustomItemSelectionListener(
                (IItemSelectionNotifier) context));
        if (selectionTitle != null) {
            add(selectionTitle);
        }
    }

    public void updateUi() {
        spinner.setAdapter(this);
        notifyDataSetChanged();
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public void setSelectedPostion(int position) {
        spinner.setSelection(position);
    }
}
