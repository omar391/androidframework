package com.astronlab.framework.android.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Extend this DatabaseCRUD class for each table as super class
 */
public class DatabaseCRUD {
    private static DatabaseHandler dbObject;
    private final String tableName;

    public DatabaseCRUD(String tableName) {
        this.tableName = tableName;
    }

    protected static void setDbHandler(DatabaseHandler dbHandler) {
        dbObject = dbHandler;
    }

    public long insertSingleRow(ContentValues values) {
        /**
         * return the row ID of the newly inserted row, or -1 if an error
         * occurred
         *
         * @param values
         *            this map contains the initial column values for the row.
         *            The keys should be the column names and the values the
         *            column values
         */
        SQLiteDatabase db = dbObject.getWritableDatabase();
        long rowId = db.insert(tableName, null, values);
        db.close();
        return rowId;
    }

    public ArrayList<ArrayList<String>> getRows(String sql,
                                                String[] selectionArgs) {
        /**
         * Runs the provided SQL and returns the result set.
         *
         * @param sql
         *            the SQL query. The SQL string must not be ; terminated
         * @param selectionArgs
         *            You may include ?s in where clause in the query, which
         *            will be replaced by the values from selectionArgs. The
         *            values will be bound as Strings.
         * @ie:
         * @rawQuery("SELECT id, name FROM people WHERE name = ? AND id = ?" ,
         *                   new String[] {"David", "2"});
         */
        SQLiteDatabase db = dbObject.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, selectionArgs);

        ArrayList<ArrayList<String>> rowList = getRowData(cursor);
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
            System.out
                    .println("Error in closing db or cursor instance. Message: "
                            + e.getMessage());
        }
        return rowList;
    }

    public ArrayList<ArrayList<String>> getAllRows() {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + tableName;

        SQLiteDatabase db = dbObject.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        ArrayList<ArrayList<String>> rowList = getRowData(cursor);
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
            System.out
                    .println("Error in closing db or cursor instance. Message: "
                            + e.getMessage());
        }
        return rowList;
    }

    private ArrayList<ArrayList<String>> getRowData(Cursor cursor) {
        ArrayList<ArrayList<String>> rowList = new ArrayList<ArrayList<String>>();

        int colNumber = cursor.getColumnCount(), itr = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ArrayList<String> rowData = new ArrayList<String>();
                itr = 0;
                while (colNumber > itr) {
                    rowData.add(cursor.getString(itr));
                    itr++;
                }
                rowList.add(rowData);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return rowList;
    }

    public int getRowsCount() {
        String countQuery = "SELECT  * FROM " + tableName;
        SQLiteDatabase db = dbObject.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        db.close();

        // return count
        return cursor.getCount();
    }

    public int updateRows(ContentValues values, String whereClause,
                          String[] whereArgs) {
        /**
         * Returns number of rows affected Convenience method for updating rows
         * in the database.
         *
         * @param values
         *            a map from column names to new column values. null is a
         *            valid value that will be translated to NULL.
         * @param whereClause
         *            the optional WHERE clause to apply when updating. Passing
         *            null will update all rows.
         * @return the number of rows affected
         */
        SQLiteDatabase db = dbObject.getWritableDatabase();

        // updating row
        int updatedRows = db.update(tableName, values, whereClause, whereArgs);
        db.close();
        return updatedRows;
    }

    public int deleteRows(String whereClause, String[] whereArgs) {
        /**
         * Return the number of rows affected if a whereClause is passed in, 0
         * otherwise. To remove all rows and get a count pass "1" as the
         * whereClause.
         *
         * @param whereClause
         *            the optional WHERE clause to apply when deleting. Passing
         *            null will delete all rows.
         */
        SQLiteDatabase db = dbObject.getWritableDatabase();
        int effectedRows = db.delete(tableName, whereClause, whereArgs);
        db.close();
        return effectedRows;
    }

    public int deleteAllRows() {
        return deleteRows(null, null);
    }
}
