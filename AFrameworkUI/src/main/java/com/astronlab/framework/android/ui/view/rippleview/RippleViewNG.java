package com.astronlab.framework.android.ui.view.rippleview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.andexert.library.RippleView;

/**
 * Usage: In an element inside a layout.xml file to make that element show ripple effect
 * i.e.
 * <com.andexert.library.RippleView
 * android:id="@+id/login_button"
 * rv_centered="true"
 * android:layout_width="wrap_content"
 * android:layout_height="wrap_content"
 * android:layout_alignParentEnd="true"
 * android:layout_alignParentRight="true"
 * android:layout_below="@+id/pass_word"
 * android:layout_marginRight="30dp"
 * android:focusable="true"
 * android:focusableInTouchMode="true"
 * android:contextClickable="true"
 * android:clickable="true">
 * <p>
 * <...other elements....></...other>
 * </p>
 * </com.andexert.library.RippleView>
 * <p>
 * Then in java file use onclick listener/or other listeners. i.e -
 * </p>
 * RippleViewNG view = (RippleViewNG) getView().findViewById(R.id.login_button);
 * view.setOnRippleCompleteListener(this);
 */
public class RippleViewNG extends RippleView {
    public RippleViewNG(Context context) {
        super(context);
    }

    public RippleViewNG(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RippleViewNG(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
