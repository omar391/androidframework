package com.astronlab.framework.android.observers.notifier;

/**
 * Use this interface in activities/fragments to track edit text's value change
 */
public interface ITextChangeNotifier {

    void onEditTextValueChange(int viewId, String newValue);
}
