package com.astronlab.framework.android.observers.notifier;

/**
 * Use this interface in activities/fragments to track edit text's value change
 */
public interface ICheckBoxChangeNotifier {

    void onCheckBoxStateChanged(int viewId, boolean isChecked);
}
