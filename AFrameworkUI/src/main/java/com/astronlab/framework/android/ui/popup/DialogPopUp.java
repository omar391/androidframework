package com.astronlab.framework.android.ui.popup;

import android.app.Dialog;
import android.content.Context;

/**
 * Custom dialog class.
 * http://www.mkyong.com/android/android-custom-dialog-example/
 * http://developer.android.com/guide/topics/ui/dialogs.html
 */
public class DialogPopUp extends Dialog {

    public DialogPopUp(Context context) {
        super(context);
    }

    public DialogPopUp(Context context, int style) {
        super(context, style);
    }

    public void setView(String title, int layout) {
        super.setContentView(layout);
        super.setTitle(title);
    }

    /**
     * If modal is true then then clicking outside of modal will not close it.
     **/
    public void setModal(boolean isModal) {
        setCancelable(!isModal);
        setCanceledOnTouchOutside(!isModal);
    }
}
