package com.astronlab.framework.android.util.sms;

import android.telephony.SmsManager;

/**
 * need to use in permission file - <uses-permission
 * android:name="android.permission.SEND_SMS"/>
 **/
public class SmsSender {

    private final String phoneNo;
    private final String message;

    public SmsSender(String no, String msg) {
        this.phoneNo = no;
        this.message = msg;
    }

    public boolean send() {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
