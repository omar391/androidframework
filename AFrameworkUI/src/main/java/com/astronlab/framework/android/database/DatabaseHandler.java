package com.astronlab.framework.android.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Make static instance of DatabaseHandler/this class for each app (as we assume
 * a single db for a single app, multiple db can be created also)
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private final String[] _databaseTablesSQL;
    /**
     * Minimum Database version is 1
     */
    private static int dbVersion = 1;
    private SQLiteDatabase db;

    public DatabaseHandler(Context context, String databaseName,
                           String... databaseTablesSQL) {
        super(context, databaseName, null, dbVersion);
        this._databaseTablesSQL = databaseTablesSQL;
    }

    public static void setDbVersion(int version) {
        dbVersion = version;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;
        crateTables();
    }

    private void crateTables() {
        for (String sql : _databaseTablesSQL) {
            try {
                if (sql != null) {
                    db.execSQL(sql);
                }
            } catch (Exception e) {
                System.out.println("Error executing sql: " + sql);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /**
         * --This method is required on - 1. Database version update 2. Database
         * schema change. -- Override it depending on your's use cases.
         * */
        this.db = db;
    }
}
