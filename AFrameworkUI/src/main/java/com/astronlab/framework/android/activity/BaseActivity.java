package com.astronlab.framework.android.activity;

import android.support.v7.app.AppCompatActivity;

import com.astronlab.framework.android.fragment.impl.OnFragmentInteractionListener;

/**
 * Created by Omar on 1/21/2016.
 */
public abstract class BaseActivity extends AppCompatActivity implements OnFragmentInteractionListener {

    @Override
    public abstract void submitSuccess();
}
