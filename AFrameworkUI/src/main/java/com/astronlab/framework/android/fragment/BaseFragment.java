package com.astronlab.framework.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astronlab.framework.android.fragment.impl.OnFragmentInteractionListener;

/**
 * Further override onCreate/onActivityCreated methods if required in all sub classes.
 * Be sure to read the docs in super Fragment class.
 */
public abstract class BaseFragment extends Fragment {
    protected OnFragmentInteractionListener interactionListener;
    public final String FRAGMENT_TAG;

    protected BaseFragment(String fragmentTag) {
        this.FRAGMENT_TAG = fragmentTag;
    }

    /**
     * Inflate a layout for the sub fragment
     */
    @Override
    public abstract View onCreateView(LayoutInflater inflater, ViewGroup container,
                                      Bundle savedInstanceState);

    @Override
    public void onDetach() {
        super.onDetach();
        interactionListener = null;
    }

    public void setListener(OnFragmentInteractionListener listener) {
        interactionListener = listener;//(OnFragmentInteractionListener) getActivity();
    }
}
