package com.astronlab.framework.android.observers.listeners;

import android.text.Editable;
import android.text.TextWatcher;

import com.astronlab.framework.android.observers.notifier.ITextChangeNotifier;


public class CustomTextChangeListener implements TextWatcher {

    private final int viewId;
    private final ITextChangeNotifier notifier;

    public CustomTextChangeListener(ITextChangeNotifier notifier, int viewId) {
        this.viewId = viewId;
        this.notifier = notifier;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        notifier.onEditTextValueChange(viewId, s.toString());
    }

}
