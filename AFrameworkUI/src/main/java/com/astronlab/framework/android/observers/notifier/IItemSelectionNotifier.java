package com.astronlab.framework.android.observers.notifier;

/**
 * Use this interface in activities/fragments to track item(spinner or others ui
 * elements) selection
 */
public interface IItemSelectionNotifier {

    void updateOnItemSelection(int viewId, int selectionIndex,
                               String value);
}
