package com.astronlab.framework.android.fragment;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.astronlab.framework.android.activity.BaseActivity;

/**
 * Created by Omar on 1/21/2016.
 */
public class FragmentUtils {

    public static void commitFragment(BaseActivity parent, BaseFragment fragment, int contentId) {
        FragmentManager fragmentManager = parent.getSupportFragmentManager();
        Fragment oldFragment = fragmentManager.findFragmentByTag(fragment.FRAGMENT_TAG);

        if (oldFragment == null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            fragment.setListener(parent);
            ft.add(contentId, fragment, fragment.FRAGMENT_TAG);
            ft.commit();
        }
    }
}
