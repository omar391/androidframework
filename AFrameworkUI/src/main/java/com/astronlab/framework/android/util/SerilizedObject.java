package com.astronlab.framework.android.util;

import java.io.Serializable;

public class SerilizedObject<E> implements Serializable {
    /**
     * use this class to send object data via intent
     */
    private static final long serialVersionUID = 1L;
    private final E object;

    public SerilizedObject(E obj) {
        this.object = obj;
    }

    public E getValue() {
        return object;
    }
}
