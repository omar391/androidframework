package com.astronlab.framework.android.ui.popup;

import android.content.Context;
import android.graphics.Color;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Check out SweetAlertDialog class for further references
 */
public class AlertDialogPopup extends SweetAlertDialog {

    public AlertDialogPopup(Context context) {
        super(context);
    }

    public AlertDialogPopup(Context context, int alertType) {
        super(context, alertType);
    }

    public void setProgressTextColor(String textColorHexCode){
        if (textColorHexCode != null) {
            getProgressHelper().setBarColor(Color.parseColor(textColorHexCode));
        }
    }

    public static AlertDialogPopup getProgressAlert(Context context, String loadingTitle, String loadingMsg) {
        AlertDialogPopup popup = setAlertType(context, PROGRESS_TYPE, loadingTitle, loadingMsg);
        popup.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        popup.setCancelable(false);

        return popup;
    }

    public static AlertDialogPopup getErrorAlert(Context context, String title, String msgText) {
        return setAlertType(context, ERROR_TYPE, title, msgText);
    }

    public static AlertDialogPopup getWarningAlert(Context context, String title, String msgText) {
        return setAlertType(context, WARNING_TYPE, title, msgText);
    }

    public static AlertDialogPopup getSuccessAlert(Context context, String title, String msgText) {
        return setAlertType(context, SUCCESS_TYPE, title, msgText);
    }

    public static AlertDialogPopup getInfoAlert(Context context, String title, String msgText) {
        return setAlertType(context, NORMAL_TYPE, title, msgText);
    }

    public static AlertDialogPopup setAlertType(Context context, int alertType, String title, String msgText) {
        AlertDialogPopup popup = new AlertDialogPopup(context, alertType);
        popup.setTitleText(title);
        popup.setContentText(msgText);

        return popup;
    }
}
