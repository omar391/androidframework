package com.astronlab.framework.android.picker;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements
        OnDateSetListener {
    private final Context context;
    private final String datePickerTag = "DATE_PICKER";
    private final int elementId;
    private String date;

    public DatePickerFragment(Context context, int elementId) {
        this.context = context;
        this.elementId = elementId;
    }

    public DatePickerFragment(Context context, int elementId, String dateStr) {
        this.context = context;
        this.elementId = elementId;
        date = dateStr; // Date str in dd-mm-yyyy format
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        Calendar c = Calendar.getInstance();
        try {
            if (date != null) {
                String[] tmp = date.split("-");
                c.set(Integer.parseInt(tmp[2]), Integer.parseInt(tmp[1]),
                        Integer.parseInt(tmp[0]));
            }
        } catch (Exception e) {
        }
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(context, this, year, month, day);
    }

    public void show(FragmentManager manager) {
        super.show(manager, datePickerTag);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear,
                          int dayOfMonth) {
        ((DatePickerDataSetListener) context).onDateSet(elementId, year,
                monthOfYear, dayOfMonth);
    }

    public interface DatePickerDataSetListener {
        void onDateSet(int elementId, int year, int monthOfYear,
                       int dayOfMonth);
    }
}
