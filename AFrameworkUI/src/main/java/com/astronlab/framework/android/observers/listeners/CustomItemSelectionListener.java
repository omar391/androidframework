package com.astronlab.framework.android.observers.listeners;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.astronlab.framework.android.observers.notifier.IItemSelectionNotifier;


/**
 * Works mainly for spinners
 */
public class CustomItemSelectionListener implements OnItemSelectedListener {

    private final IItemSelectionNotifier updateListener;

    public CustomItemSelectionListener(IItemSelectionNotifier updateListener) {
        this.updateListener = updateListener;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos,
                               long id) {
        int viewId = parent.getId();
        updateListener.updateOnItemSelection(viewId, pos, parent
                .getItemAtPosition(pos).toString().trim());
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }
}

// public interface ItemSelection