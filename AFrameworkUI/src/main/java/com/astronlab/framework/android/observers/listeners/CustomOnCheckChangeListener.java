package com.astronlab.framework.android.observers.listeners;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.astronlab.framework.android.observers.notifier.ICheckBoxChangeNotifier;


public class CustomOnCheckChangeListener implements OnCheckedChangeListener {

    private final int viewId;
    private final ICheckBoxChangeNotifier notifier;

    public CustomOnCheckChangeListener(ICheckBoxChangeNotifier notifier,
                                       int viewId) {
        this.viewId = viewId;
        this.notifier = notifier;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        notifier.onCheckBoxStateChanged(viewId, isChecked);
    }
}
