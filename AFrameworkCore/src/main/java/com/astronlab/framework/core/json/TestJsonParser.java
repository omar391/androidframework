package com.astronlab.framework.core.json;

import com.astronlab.json.reader.RegexJsonParser;

import java.io.IOException;

/**
 * Created by Omar on 1/13/2016.
 */
public class TestJsonParser {

    public String parseMe() throws IOException {
        String s = "{\"id\":\"2\",\"employe_id\":\"1076\",\"email\":\"admin@admin.com\",\"name\":\"Admin\",\"department\":\"\",\"designation\":\"\",\"contact\":\"\",\"picture\":\"\",\"created_at\":\"2015-11-26 10:01:27\",\"updated_at\":\"2016-01-13 15:12:17\",\"role\":\"2\",\"permissions\":\"{\\\"User\\\":{\\\"menu\\\":\\\"1\\\",\\\"create\\\":\\\"1\\\",\\\"read\\\":\\\"1\\\",\\\"update\\\":\\\"1\\\",\\\"delete\\\":\\\"1\\\"},\\\"Category\\\":{\\\"menu\\\":\\\"1\\\",\\\"create\\\":\\\"1\\\",\\\"read\\\":\\\"1\\\",\\\"update\\\":\\\"1\\\",\\\"delete\\\":\\\"1\\\"},\\\"Items\\\":{\\\"menu\\\":\\\"1\\\",\\\"create\\\":\\\"1\\\",\\\"read\\\":\\\"1\\\",\\\"update\\\":\\\"1\\\",\\\"delete\\\":\\\"1\\\"},\\\"Company\\\":{\\\"menu\\\":\\\"1\\\",\\\"create\\\":\\\"1\\\",\\\"read\\\":\\\"1\\\",\\\"update\\\":\\\"1\\\",\\\"delete\\\":\\\"1\\\"},\\\"Role\\\":{\\\"menu\\\":\\\"1\\\",\\\"create\\\":\\\"1\\\",\\\"read\\\":\\\"1\\\",\\\"update\\\":\\\"1\\\",\\\"delete\\\":\\\"1\\\"},\\\"Report\\\":{\\\"menu\\\":\\\"1\\\",\\\"read\\\":\\\"1\\\",\\\"delete\\\":\\\"1\\\"}}\",\"api_access_key\":\"af1db61b8bd93469cc7562973df54101\"}\n";

        RegexJsonParser jsonParser = new RegexJsonParser(s);

        return jsonParser.getSingleValueByKey("api_access_key");
    }
}
