package com.astronlab.framework.core.rx;

import com.astronlab.rx.observable.RxObservable;
import com.astronlab.rx.observer.RxOnSubscribe;
import com.astronlab.rx.subscriber.RxSubscriber;

import rx.observers.SafeSubscriber;

/**
 * Created by Omar on 1/19/2016.
 */
public class RxSample implements RxOnSubscribe {
    RxObservable rxObservable = new RxObservable(this);

    public static void main(String[] args) {
        RxSample sampleRx = new RxSample();
        sampleRx.test();
    }

    void test() {
        MySubscriber subscriber = new MySubscriber();

        //On subscribe way
        rxObservable.subscribe(subscriber);

        //Static way without on-subscribe
        RxObservable.just("Ops mannn..it's static-2").subscribe(subscriber);
    }

    @Override
    public void call(Object o) {
        System.out.println("Just subscribed-1");
        ((SafeSubscriber) o).onNext("Yahoooo!!-1");
    }
}

class MySubscriber<T> extends RxSubscriber {

    @Override
    public void onCompleted() {
        System.out.println("Just completed");
    }

    @Override
    public void onNext(Object o) {
        System.out.println("Subscription result arrived: " + o.toString());
    }
}