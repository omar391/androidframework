package com.astronlab.framework.core.http;

import com.astronlab.ngenhttplib.http.HttpInvoker;
import com.astronlab.rx.observable.RxObservable;
import com.astronlab.rx.subscriber.RxSubscriber;
import com.astronlab.rx.thread.RxThreadScheduler;

import java.util.Calendar;

/**
 * Created by Omar on 1/13/2016.
 */
public class SampleHttp {
    HttpInvoker httpInvoker = new HttpInvoker();

    public static void main(String[] args) throws Exception {

        SampleHttp sampleHttp = new SampleHttp();
        sampleHttp.runMe();

    }

    private void runMe() throws Exception {
        RxObservable.just(downloadData())
                .observeOn(RxThreadScheduler.instance())
                .subscribe(new MySubs());
        System.out.println("Thread: " + Thread.currentThread().getName() + " Time: " + Calendar.getInstance().getTime().toString());
    }

    private String downloadData() throws Exception {
        httpInvoker.config().setUrl("http://yahoo.com");
        return httpInvoker.getStringData();
    }
}


class MySubs extends RxSubscriber {

    @Override
    public void onCompleted() {
        System.out.println("Done on thread: " + Thread.currentThread().getName());
    }

    @Override
    public void onNext(Object o) {
        System.out.println("Got result" + " Time: " + Calendar.getInstance().getTime().toString());
    }
}